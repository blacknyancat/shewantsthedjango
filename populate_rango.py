import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')

import django
django.setup()

from rango.models import Category, Page

def populate():
	python_cat = add_cat('Python',views=128,likes=64)
	
	add_page(cat=python_cat,
		title="Official Python Tutorial",
		url="http://docs.python.org/2/tutorial/",
		views=123)
		
	add_page(cat=python_cat,
		title="How to think like a computer scientist",
		url="http://www.korokithakis.net/tutorials/python/",
		views= 12)
		
	django_cat = add_cat("Django",views=64,likes=32)
	
	add_page(cat=django_cat,
		title="Official Django Tutorial",
		url="https://docs.djangoproject.com/en/1.5/intro/tutorial01/",
		views=932)
		
	add_page(cat=django_cat,
		title="Django Rocks",
		url="http://www.tangowithdjango.com/",
		views=51)
		
	frame_cat = add_cat("Other Frameworks",views=32,likes=16)
	
	add_page(cat=frame_cat,
		title="Bottle",
		url="http://bottlepy.org/docs/dev/",
		views=62)
		
	add_page(cat=frame_cat,
		title="Flask",
		url="http://flask.pocoo.org",
		views=34)
		
	for c in Category.objects.all():
		for p in Page.objects.filter(category=c):
			print "- {0} - {1} ".format(str(c), str(p))
	
def add_page(cat, title, url, views=0):
	p = Page.objects.update_or_create(category=cat, title=title, url=url, views=views)[0]
	
def add_cat(name,views=0,likes=0):
	c = Category.objects.get_or_create(name=name,views=views, likes=likes)[0]
	return c
	
	
if __name__ == '__main__':
	print "Starting Rango population script..."
	populate()
	
		
